package com.example.victor.timerexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button bAction;
    TextView tAlert;

    static final long TIME = 5; // время за которое нужно успеть нажать на кнопку
    long currentTime = 0; // Количество секунд
    volatile boolean isPressed = false; // нажали или нет
    volatile boolean isSuccess = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bAction = (Button) findViewById(R.id.b_action);
        tAlert = (TextView) findViewById(R.id.tv_alert);

        bAction.setOnClickListener(this);

        Timer timer = new Timer(); // на объект таймера могут подключаться разные таски

        TimerTask task = new TimerTask() { // задача которую нужно выполнить
            @Override
            public void run() {
                // тут описание действий которые нужно выполнять
                // это параллельный поток, обращаться к юзер интерфейсу напрямую нельзя
                currentTime++; // задача выполняется каждую секунду, считаем секунды
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() { // обновление секундомера
                        tAlert.setText(String.valueOf(TIME - currentTime) + " секунд осталось");
                    }
                });

                if (currentTime >= TIME) { // если время вышло завершаем задачу
                    runOnUiThread(new Runnable() { // это способ безопасно обратиться к UI, еще есть Handler
                        @Override
                        public void run() {
                            tAlert.setText("НЕ УСПЕЛ");
                        }
                    });
                    cancel(); // завершаем задачу
                }
                if (isPressed) { // если кнопку нажали
                    isSuccess = true; // поднимаем флажок
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tAlert.setText("УСПЕЛ");
                        }
                    });
                    //cancel(); // завершаем задачу
                }


            }
        };

        timer.schedule(task, // что за задача выполняется
                        0,   // задержка, 0 значит выполнить сразу
                        1000  // переодичность выполнения в милисекундах
                        );



    }

    @Override
    public void onClick(View view) {
        isPressed = true;
        //tAlert.setText("CLICK");
    }
}
